# CLP - Repository Management

The **Repository Management** component consists of the management and the storage of all platform data.

[<img src="/rp-arch__1_.png" width="550"/>](/rp-arch__1_.png)

The component includes the following modules:
- **Importer manager**: manages all messages arriving from the Data Collection, that is, all those relating to the importation of a new dataset;
- **Config Manager**: manages all messages regarding the addition of a new configuration. Each time a new configuration is added, all the datasets present in the platform must be standardized according to its specifications, this to keep the entire CLP aligned;
- **Uniformer manager**: deals with the management of communication between the Repository Manager and the Data Management component;
- **Query manager**: manages the incoming messages from the Data Distribution regarding the requests for data download by the user. It forwards the requests to the database which returns the requested data. Then it saves them in a JSON file.


## APIs

- **/dbToBeUniformed [GET]** : returns the list of datasets not uniformed present in the platform.

- **/labelComparison/\<dbName\> [GET]** : returns a JSON structure containing the comparison between the labels of the uniformed dataset and the labels of the dataset \<dbName\>.

- **/config [GET]** : returns the list of all configurations in the platform.

- **/dbNames [GET]** : returns the list of the datasets uniformed.

- **/labels [GET]** : returns the list of the activities present in the uniformed dataset.

- **/graphsComparison/\<dbName\> [GET]** : returns a JSON structure containing the magnitude graph comparison (in BASE64) between the labels of the uniformed dataset and the labels of the dataset \<dbName\>.