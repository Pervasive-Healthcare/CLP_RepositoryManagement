import os
import glob
import json
import threading
import yaml

from flask import Flask
from flask_cors import CORS

from Core.manager.config_manager import on_get_config_received
from Core.MongoDB.mongo_repository import init_db_connection, create_default_config, on_get_dbnames_received, \
    on_get_labels_received
from Core.manager.label_manager import on_get_db_ready_to_bo_uniformed_received, on_get_labels_comparison_received, on_get_graph_comparison_received
from Core.RabbitMQ.rabbitmq_manager import init_rabbitmq
from Utils.enums import Config


with open("config.yml", "r") as yml_file:
    config = yaml.safe_load(yml_file)

init_db_connection()
create_default_config()

app = Flask(__name__)
CORS(app)


def flask_thread():
    app.run(host="0.0.0.0", port=5001)


#######################################################################################################################
#                                        /db_to_be_uniformed
#######################################################################################################################

@app.route("/dbToBeUniformed", methods=["GET"])
def trigger_get_uniformed_dataset():
    return on_get_db_ready_to_bo_uniformed_received()


#######################################################################################################################
#                                      /labelComparison/<db_name>
#######################################################################################################################

@app.route("/labelComparison/<db_name>", methods=["GET"])
def trigger_get_label_comparison(db_name):
    return on_get_labels_comparison_received(db_name)


#######################################################################################################################
#                                      /config/<db_name>
#######################################################################################################################

@app.route("/config", methods=["GET"])
def trigger_get_config():
    return json.dumps(on_get_config_received())


#######################################################################################################################
#                                          /dbNames
#######################################################################################################################

@app.route("/dbNames", methods=["GET"])
def trigger_get_db_names():
    return json.dumps(on_get_dbnames_received())


#######################################################################################################################
#                                         /labels
#######################################################################################################################

@app.route("/labels", methods=["GET"])
def trigger_get_labels():
    return json.dumps(on_get_labels_received())

#######################################################################################################################
#                                      /graphsComparison/<db_name>
#######################################################################################################################

@app.route("/graphsComparison/<db_name>", methods=["GET"])
def trigger_get_graph_comparison(db_name):
    return on_get_graph_comparison_received(db_name)

#######################################################################################################################
#                                        /download_json
#######################################################################################################################

@app.route("/download_json/<json_filename>", methods=["GET"])
def download_json(json_filename):
    print("request")
    #dir_path = os.path.join(app.root_path, 'Data\QueryResult', json_filename)
    dir_path = os.path.join(app.root_path, 'Data', 'QueryResult', json_filename)
    print(dir_path)
    list_of_files = glob.glob(dir_path)
    latest_file = max(list_of_files, key=os.path.getctime)

    f = open(latest_file,)

    data = json.load(f)

    return data

#######################################################################################################################
#                                               MAIN
#######################################################################################################################
if __name__ == "__main__":
    thread = threading.Thread(target=flask_thread)
    thread.start()

    init_rabbitmq(config[Config.RABBIT_MQ_CONNECTION.value])



