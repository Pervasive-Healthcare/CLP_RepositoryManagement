from Core.MongoDB.mongo_repository import save_data, save_dbinfo, get_default_config, \
    get_all_data_to_be_uniformed_by_id, update_dbinfo_status

import yaml
import pickle
from pymongo import MongoClient
from Utils.enums import Config


# 1. dbinfo received from importer
from Core.RabbitMQ.message_sender import send_message



def on_dbinfo_received(db_info):
    db_info["db_id"] = db_info.get("db_name")
    db_info["status"] = 0  # imported ma non uniformed default
    save_dbinfo("dbinfo_to_be_uniformed", db_info)
    print("Save imported data (%s)" % db_info.get("db_name"))


# 2. Single data sample received from importer
def on_imported_data_received(imported_data):
    imported_data["db_id"] = imported_data.get("db_name")
    save_data(imported_data, "imported_temp")


# 3. All data of a db has been received. Once finished to save in DB send end_importing_message to data manager
def on_data_end_importing_received(end_importing):
    print("End Importing " + end_importing.get("db_name"))
    end_importing_msg = {"type": "end_importing_default", "db_id": end_importing.get("db_name")}
    send_message('data_to_dm', end_importing_msg)
    #producer.send(KafkaTopic.RESPONSE.value, end_importing_msg)


# 5. Send all imported data of a given db to data manager (without considering all config). At this point we want
# to uniform dataset only with a standard frequency
def on_imported_data_default_request_received(msg):
    db_id = msg.get("db_id")
    all_data, dbinfo = get_all_data_to_be_uniformed_by_id(db_id)
    default_config = get_default_config()

    for data in all_data:
        data['type'] = 'to_uniform_default'
        data['sensors'] = dbinfo.get('sensors')
        data['config'] = default_config

        send_message('data_to_dm', data)

    msg = {
        'type': 'end_to_send_imported_data',
        'db_id': db_id,
        'db_info': dbinfo
    }
    send_message('data_to_dm', msg)


# 7. Receive and store in temp collection uniformed data with default config with feature extracted
def on_uniformed_default_data_received(data):
    save_data(data, "uniformed_frequency")


# 9. end uniform default received
def on_end_uniformed_default_data_received(data):
    dbinfo = data.get('db_info')
    dbinfo["status"] = 1
    update_dbinfo_status(dbinfo)

# 10. normalize uniformed dataset
def normalize_data_received(data_info):
    
    # Crate connection with DB
    with open("config.yml", 'r') as yml_file:
        config = yaml.safe_load(yml_file)
    mongo_connection_data = config[Config.MONGO_CONNECTION.value]
    client = MongoClient(mongo_connection_data[Config.HOST.value], mongo_connection_data[Config.PORT.value])
    db_connection = client[mongo_connection_data[Config.DB_NAME.value]]

    data = db_connection["config"].find()
    uniformed = ['uniformed_'+str(row['id']) for row in data]
        
    for uniformed_ in uniformed:
        num_imported =db_connection["imported"].find({"db_id": data_info['db_id']}).count(True)
        num_uniformed = db_connection[uniformed_].find({"db_id": data_info['db_id']}).count(True)

        if num_imported != num_uniformed:
            return
        print("\nNormalizing data of DB ", data_info['db_id'], '(',uniformed_,')')

        db_connection["norm_data"].insert_one({ "db_id": data_info['db_id']})    

        # Get sensors names
        sensors_full = data = db_connection["dbinfo"].find({"db_id": data_info['db_id']})[0]['sensors']
        sensors = []
        for tmp in sensors_full:
            sensors.append(tmp['name'])
        print("Sensors: ", sensors)

        data = list(db_connection[uniformed_].find({"db_id": data_info['db_id']}))
        print("Number of collections:", len(data) )

        # Process of normalization data
        try:
            for sensor in sensors:
                print("Normalizing data of sensor: ", sensor)
                x = []
                y = []
                z = []
                for single_data in data:
                    for tmp in single_data[sensor]:
                        x.append(tmp[0])
                        y.append(tmp[1])
                        z.append(tmp[2])

                x_min = min(x)
                x_max = max(x)
                y_min = min(y)
                y_max = max(y)
                z_min = min(z)
                z_max = max(z)

                myquery = { "db_id": data_info['db_id'] }
                newvalues = { "$addToSet": { sensor : {"x_min" :x_min, "x_max":x_max, "y_min":y_min, "y_max":y_max, "z_min":z_min, "z_max":z_max } } }
                db_connection["norm_data"].update_one(myquery, newvalues)

                for i in range(len(data)):
                    for j in range(len(data[i][sensor])):
                        data[i][sensor][j][0] = (data[i][sensor][j][0] - x_min)/(x_max-x_min)
                        data[i][sensor][j][1] = (data[i][sensor][j][1] - y_min)/(y_max-y_min)
                        data[i][sensor][j][2] = (data[i][sensor][j][2] - z_min)/(z_max-z_min)
            print("Normalization process done!")
        except Exception as e:
            print("Error during normalization process!")
            print(e)
            pass

        # Update normalized data in db
        try:
            for i in range(len(data)):
                id_ = data[i]['_id']
                for sensor in sensors:
                    db_connection[uniformed_].update_one(
                        { "_id" : id_ },
                        { "$set":{ sensor : data[i][sensor], sensor : data[i][sensor]} }
                    )
            print("End: Correct update of normalized data!\n")
        except Exception as e:
            print("EndError in updating normalized data!\n")
            print(e)
            pass


# 11. normalize activity in the same range of values
def normalize_range(data_info):
    # Crate connection with DB
    with open("config.yml", 'r') as yml_file:
        config = yaml.safe_load(yml_file)
    mongo_connection_data = config[Config.MONGO_CONNECTION.value]
    client = MongoClient(mongo_connection_data[Config.HOST.value], mongo_connection_data[Config.PORT.value])
    db_connection = client[mongo_connection_data[Config.DB_NAME.value]]


    data = db_connection["config"].find()
    uniformed = ['uniformed_'+str(row['id']) for row in data]

    activities = db_connection["dbinfo"].find({'db_id':data_info['db_id']})[0]['activities']
        
    for uniformed_ in uniformed:
        num_imported =db_connection["imported"].find({"db_id": data_info['db_id']}).count(True)
        num_uniformed = db_connection[uniformed_].find({"db_id": data_info['db_id']}).count(True)

        if num_imported != num_uniformed:
            return 1
        #print("\nNormalizing data of DB ", data_info['db_id'], '(',uniformed_,')')

        # Get sensors names
        sensors_full = data = db_connection["dbinfo"].find({"db_id": data_info['db_id']})[0]['sensors']
        sensors = []
        for tmp in sensors_full:
            sensors.append(tmp['name'])
        #print("Sensors: ", sensors)

        for activity in activities:

            #print("Activity: ", activity)
            data = list(db_connection[uniformed_].find({"db_id": data_info['db_id'], "label":activity}))
            #print("Number of collections:", len(data) )

            try:
                label_exists = db_connection["norm_data"].find({"label":activity})[0]
                labelExists = True
            except:
                labelExists = False
            
            if labelExists:
                for sensor in sensors:
                    norm = db_connection["norm_data"].find({"label":activity})[0][sensor][0]
                    x_min = norm['x_min']; y_min = norm['y_min']; z_min = norm['z_min']
                    x_max = norm['x_max']; y_max = norm['y_max']; z_max = norm['z_max']; 
                    #print(norm)

                    for i in range(len(data)):
                        for j in range(len(data[i][sensor])):
                            data[i][sensor][j][0] = x_min - (x_max - x_min) * data[i][sensor][j][0]
                            data[i][sensor][j][1] = y_min - (y_max - y_min) * data[i][sensor][j][1]
                            data[i][sensor][j][2] = z_min - (z_max - z_min) * data[i][sensor][j][2]
                
                try:
                    for i in range(len(data)):
                        id_ = data[i]['_id']
                        for sensor in sensors:
                            db_connection[uniformed_].update_one(
                                { "_id" : id_ },
                                { "$set":{ sensor : data[i][sensor], sensor : data[i][sensor]} }
                            )
                except Exception as e:
                    print(e)
                    pass

            else:
                db_connection["norm_data"].insert_one({ "label": activity})
                # Process of normalization data
                try:
                    for sensor in sensors:
                        #print("Normalizing data of sensor: ", sensor)
                        x = []
                        y = []
                        z = []

                        for single_data in data:
                            for tmp in single_data[sensor]:
                                x.append(tmp[0])
                                y.append(tmp[1])
                                z.append(tmp[2])

                        x_min = min(x)
                        x_max = max(x)
                        y_min = min(y)
                        y_max = max(y)
                        z_min = min(z)
                        z_max = max(z)

                        myquery = { "label": activity }
                        newvalues = { "$addToSet": { sensor : {"x_min" :x_min, "x_max":x_max, "y_min":y_min, "y_max":y_max, "z_min":z_min, "z_max":z_max } } }
                        db_connection["norm_data"].update_one(myquery, newvalues)

                except Exception as e:
                    print(e)
                    pass