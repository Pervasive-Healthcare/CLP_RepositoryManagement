import json 

db_connection_mongo = None

#Impostazione della connessione al DB
def setup_connection_model_manager(db_connection):
	global db_connection_mongo
	db_connection_mongo = db_connection


def get_model(request):
	#La richiesta ricevuta dal DataDistribution contiene nella chiave 'query_gen' la query di
	#cui si sta verificando se esiste o no un modello
	query = request['query_gen']
	db_models = db_connection_mongo['models_manager'].find_one({"query_gen": query})

	if db_models:
		db_models.pop("_id")
		return {"results": "ok", "model_info": db_models}

	return {"results": "error"}


def put_model(request):
	document = request
	
	try:
		document.pop("type") #Rimuovo l'informazione relativa al tipo di richiesta in quanto non
		                     #deve essere prensente nel DB.
		document["query_gen"].pop("new_model")
	except KeyError:
		pass

	result = db_connection_mongo['models_manager'].insert_one(document)
	
	if result.inserted_id:
		return {"results":"ok"}

	return {"results": "error"}
