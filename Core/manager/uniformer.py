import math
import scipy.signal as sps
from scipy import signal
import pandas as pd
import pickle

G = 9.80665
N = 57.29

# Parameters for Butterworth filter
LOWCUT = 0.5
HIGHCUT = 24    # TOCHECK
FS = 50
ORDER = 5
BTYPE = 'bandpass'


def start_uniform(sensor, values, default_frequency, default_uom):
    uniformed = uniform_uom(sensor.get("name"), sensor.get("uom"), values, default_uom)
    return uniformed


# UNIFORM UOM
def uniform_uom(sensor, current_uom, values, default_uom):

    if sensor == 'accelerometer' and current_uom != default_uom:
        #g --> m/s^2
        if current_uom == 'g' and default_uom == 'm/s^2':
            new_acc = []
            for acc in values:
                new_acc.append(acc * G)
            return remove_gravity(new_acc)
        
        # m/s^2 --> g
        if current_uom == 'm/s^2' and default_uom == 'g':
            new_acc = []
            for acc in values:
                new_acc.append(acc / G)
            return remove_gravity(new_acc)
    
    if sensor == 'gyroscope' and current_uom != default_uom:
        #rad/s --> degree/s
        if current_uom == 'rad/s' and default_uom == 'degree/s':
            new_gyr = []
            for gyr in values:
                new_gyr.append(gyr * N)
            return remove_gravity(new_gyr)
        
        # degree/s --> rad/s
        if current_uom == 'degree/s' and default_uom == 'rad/s':
            new_gyr = []
            for gyr in values:
                new_gyr.append(gyr / N)
            return remove_gravity(new_gyr)

    return remove_gravity(values)

# FILTER BUTTERWORTH
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq

    b, a = signal.butter(order, [low, high], btype=BTYPE)
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = signal.lfilter(b, a, data)
    return y

def remove_gravity(values):
  x = [ acc for acc in values]
  filtered_x = butter_bandpass_filter(data = x, lowcut = LOWCUT, highcut = HIGHCUT, fs = FS)
  
  return filtered_x