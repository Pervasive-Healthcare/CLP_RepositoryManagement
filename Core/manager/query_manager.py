from datetime import datetime
import os
import json

db_connection_mongo = None


def setup_connection_query_repo(db_connection):
    global db_connection_mongo
    db_connection_mongo = db_connection


def on_query_request_received(query_info):
    query = {}
    
    configuration = query_info.get("configuration")
    collection = "uniformed" + "_" + str(configuration)
    #collection = "imported"

    db_names_or_list = []
    sensor_or_list = []
    label_or_list = []
    position_or_list = []
    device_or_list = []

    # -------------------------------------------------- DB_NAMES -----------------------------------------------------
    db_names = query_info.get("db_names")
    print("db_names: " + str(db_names))
    if db_names:
        for db_name in db_names:
            db_names_or_list.append(db_name)
        query["db_id"] = {"$in": db_names_or_list}

    # -------------------------------------------------- SENSORS -----------------------------------------------------
    
    sensors = query_info.get("sensors")
    print("sensors: " + str(sensors))
    if sensors:
        for sensor in sensors:
            sensor_or_list.append(sensor)
        query["sensors"] = {"$elemMatch": {"type": {"$in": sensor_or_list}}}
        
    # --------------------------------------------------- LABELS -----------------------------------------------------
    labels = query_info.get("labels")
    print("labels: " + str(labels))
    if labels:
        for label in labels:
            label_or_list.append(label)
        query["label"] = {"$in": label_or_list}

    # --------------------------------------------------- POSITIONS ----------------------------------------------------
    positions = query_info.get("positions")
    print("positions: " + str(positions))
    if positions:
        for position in positions:
            position_or_list.append(position)
        query["position"] = {"$in": position_or_list}

    # ---------------------------------------------------- DEVICES -----------------------------------------------------
    devices = query_info.get("devices")
    print("devices: " + str(devices))
    if devices:
        for device in devices:
            device_or_list.append(device)
        query["device"] = {"$in": device_or_list}

    # ------------------------------------------------------ USERS ------------------------------------------------------
    users = query_info.get("users")
    print("users: " + str(users))
    if users:
        age = users.get("age")
        weight = users.get("weight")
        height = users.get("height")
        sex = users.get("sex")

        if age:
            age_range = get_range(age)
            query["user.age"] = {"$gte": age_range[0], "$lte": age_range[1]}
        if weight:
            weight_range = get_range(weight)
            query["user.weight"] = {"$gte": weight_range[0], "$lte": weight_range[1]}
        if height:
            height_range = get_range(height)
            query["user.height"] = {"$gte": height_range[0], "$lte": height_range[1]}
        if sex:
            query["user.sex"] = sex

    # -----------------------------------------------------------------------------------------------------------------
    my_coll = db_connection_mongo[collection]
    
    data_to_json = {}

    #Query for uniformed_0 database
    results = list(my_coll.find(query))
    
    #Building results on json object
    data_to_json["sensors"] =  []
    data_to_json["labels"] =  labels
    
    if positions:
        data_to_json["positions"] = positions
    
    if devices:
        data_to_json["devices"] = devices
    
    if users:
        data_to_json["users"] = users
    
    data_to_json["data"] = []
    k=0

    #if results are null, find only sensors values and insert them in JSON file
    if not results:
        query_sensors = list(my_coll.find({}, {"accelerometer":False,"gyroscope":False,"magnetometer":False, "orientation":False}))
        sensors = query_sensors[0]["sensors"]
        data_to_json["data"] = []
    
    for i in results:
        data_to_json["data"].append({})
        data_to_json["data"][k]["label"] = i["label"]
        data_to_json["data"][k]["position"] = i["position"]
        data_to_json["data"][k]["device"] = i["device"]
        data_to_json["data"][k]["user"] = i["user"]
        data_to_json["data"][k]["db_id"] = i["db_id"]
        k = k+1

    if "accelerometer" in sensor_or_list:
        if results:
            k=0
            data_to_json["sensors"].append(results[0]["sensors"][0])
            for i in results:
                data_to_json["data"][k]["accelerometer"] = i["accelerometer"]
                k+=1
        else:
            for s in sensors:
                if s["type"] == "accelerometer":
                    data_to_json["sensors"].append(s)
        
    if "gyroscope" in sensor_or_list:
        if results:
            k=0
            data_to_json["sensors"].append(results[0]["sensors"][1])
            for i in results:
                data_to_json["data"][k]["gyroscope"] = i["gyroscope"]
                k+=1
        else:
            for s in sensors:
                if s["type"] == "gyroscope":
                    data_to_json["sensors"].append(s)

    if "magnetometer" in sensor_or_list:
        if results:
            k=0
            data_to_json["sensors"].append(results[0]["sensors"][2])
            for i in results:
                data_to_json["data"][k]["magnetometer"] = i["magnetometer"]
                k+=1
        else:
            for s in sensors:
                if s["type"] == "magnetometer":
                    data_to_json["sensors"].append(s)

    if "orientation" in sensor_or_list:
        if results:
            k=0
            data_to_json["sensors"].append(results[0]["sensors"][2])
            for i in results:
                data_to_json["data"][k]["orientation"] = i["orientation"]
                k+=1
        else:
            for s in sensors:
                if s["type"] == "orientation":
                    data_to_json["sensors"].append(s)

    #directory checking
    query_json_file = "Data/QueryResult"
    if not os.path.exists(query_json_file):
        os.makedirs(query_json_file)

    #date parsing 
    dt_string = datetime.now().strftime("%d-%m-%Y-%H-%M-%S")
    
    #save file path to return
    file_name = "query_" + str(dt_string) + ".json"
    file_path = "Data/QueryResult/" + file_name 

    #save data on JSON file
    with open(file_path, "w") as f:
            f.write(json.dumps(data_to_json))

    request = {}
    request["sensors"] = data_to_json["sensors"]
    request["labels"] = query_info.get("labels")
    request["positions"] = query_info.get("positions")
    request["devices"] = query_info.get("devices")
    request["users"] = query_info.get("users")
    request["configuration"] = query_info.get("configuration")
    request["date"] = dt_string
    request["db_names"] = query_info.get("db_names")
    db_connection_mongo["requests"].insert_one(request)

    print("Query completed and saved (num record: " + str(len(data_to_json["data"])) + ")")

    return file_name

def get_range(data):
    split = data.split("-")
    split[0] = int(split[0])
    split[1] = int(split[1])
    return split
