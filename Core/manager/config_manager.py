from Core.MongoDB.mongo_repository import insert_new_config, get_sensor_by_db_id, get_all_dbinfo, get_list_data, \
    get_all_config
from Core.RabbitMQ.message_sender import send_message


def on_get_config_received():
    all_config = get_all_config()
    configs = []

    for config in all_config:
        temp = {
            'id': config.get('id'),
            'frequency': config.get('frequency'),
            'uom': config.get('uom')
        }
        configs.append(temp)

    return configs


def config_already_exist(msg):
    # TODO control if the config already exist in the DB
    return False


def on_new_config_request_received(msg):
    if config_already_exist(msg):
        print("config already exist (" + msg + ")")
        return

    new_config = insert_new_config(msg)
    dbinfo = get_all_dbinfo()

    for db in dbinfo:
        all_data = get_list_data('imported', db.get('db_id'))
        for data in all_data:
            data['type'] = 'to_uniform'
            data['config'] = new_config
            data['sensors'] = get_sensor_by_db_id(data.get('db_id')).get('sensors')
            send_message('data_to_dm', data)
