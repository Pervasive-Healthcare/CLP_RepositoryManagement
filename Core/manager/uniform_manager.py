
from random import randrange

from Core.MongoDB.mongo_repository import delete_all_data, insert_one, \
    get_all_data_to_be_uniformed_by_id, get_all_config, save_data, get_all_data_by_id_uniformed, \
    get_list_data, find_one_and_delete_labels

import numpy as np
from Core.RabbitMQ.message_sender import send_message

# 1. Receive from the importer the mapping between old_label-new_label
def on_uniform_request_received(msg):
    db_id = msg.get('db_id')
    all_data, db_info = get_all_data_to_be_uniformed_by_id(db_id)
    delete_all_data(db_id, "dbinfo_to_be_uniformed")
    delete_all_data(db_id, "imported_temp")

    for data in all_data:
        current_label = data.get('label')
        if data.get('db_name'):
            del data['db_name']
        data['old_label'] = current_label
        data['label'] = msg.get(current_label)
        insert_one("imported", data)

    if db_info.get('db_name'):
        del db_info['db_name']
    if db_info.get('type'):
        del db_info['type']

    if msg.get('db_id'):
        del msg['db_id']
    if msg.get('type'):
        del msg['type']

    db_info['old_activities'] = db_info['activities']
    db_info['activities'] = list(msg.values())

    db_info['mapping_labels'] = msg
    insert_one("dbinfo", db_info)

    update_feature_for_activity(db_info)

    delete_all_data(db_id, "uniformed_frequency")

    end_importing = {"type": "end_to_uniform_labels", "db_id": db_id}
    send_message('res_to_dm', end_importing)
    # producer.send(KafkaTopic.RESPONSE.value, end_importing)
    print("--> Send end_to_uniform_labels message for db", db_id)
    # Start to uniform foreach config


# 3. Prepare to send data to uniformer for uniform frequency in accord to all config
def on_imported_data_request_received(msg):
    db_id = msg.get("db_id")

    all_data, dbinfo = get_all_data_by_id_uniformed(db_id)
    configs = get_all_config()

    for data in all_data:
        data['type'] = 'to_uniform'
        data['sensors'] = dbinfo.get('sensors')

        for config in configs:
            data['config'] = config
            send_message('data_to_dm', data)

    msg = {
        'type': 'end_to_send_imported_data',
        'db_id': db_id
    }
    send_message('data_to_repo', msg)


# 5. Receive uniformed data and store in DB
def on_uniformed_data_received(data):
    config_id = str(data.get("config_id"))
    save_data(data, "uniformed_%s" % config_id)




# 1.1 Aggiorna la media delle feature della collection 'labels' aggiungendo eventualmente nuove feature
def update_feature_for_activity(db_info):
    data = get_list_data('uniformed_frequency', db_info.get('db_id'))
    mapping = db_info.get('mapping_labels')
    old_labels = db_info.get('old_activities')

    labels = find_one_and_delete_labels()
    if not labels:
        labels = {}

    for activity in old_labels:        
        data_by_act = [sample for sample in data if sample.get('label') == activity]
        fe = []
        for sample in data_by_act:
            if isinstance(sample.get('fe'), list):
                fe.append(sample.get('fe'))

        if len(fe) != 0:
            feature_avg = np.array(fe)
            current_feature_avg = np.mean(feature_avg, axis=0).tolist() #.reshape((1,6)).

            current_act = labels.get(mapping[activity])
            if not current_act:
                labels[mapping[activity]] = {
                    'count': len(data_by_act),
                    'fe_avg': current_feature_avg,
                    'magnitude': data_by_act[randrange(len(data_by_act) - 1)].get('magnitude')
                }
            else:
                count = current_act.get('count')
                fe_avg = current_act.get('fe_avg')

                avg_updated = list(map(lambda x: x*count, fe_avg))
                current_fe_sum = list(map(lambda x: x*len(data_by_act), current_feature_avg))

                for i, avg in enumerate(avg_updated):
                    avg_updated[i] = avg_updated[i] + current_fe_sum[i]

                avg_updated = list(map(lambda x: x / (count + len(data_by_act)), avg_updated))

                labels[mapping[activity]] = {
                    'count': count + len(data_by_act),
                    'fe_avg': avg_updated,
                    'magnitude': data_by_act[randrange(len(data_by_act)-1)].get('magnitude')
                }

    save_data(labels, 'labels')





