import os
import math
import numpy as np
import pathlib
import matplotlib
import seaborn as sns
from random import randrange
import matplotlib.pyplot as plt
import base64
import json
import string

from Core.MongoDB.mongo_repository import get_all_dbinfo, get_all_list_data, get_all_data_to_be_uniformed, \
    get_all_labels
from Core.manager.uniformer import start_uniform, remove_gravity

matplotlib.use('Agg')


EUCLIDEAN_THRESHOLD = 3
EDIT_DISTANCE_THRESHOLD = 20

head = pathlib.Path().absolute()
graph_path = os.path.join(head, "Data", "Graph")


# Calculate fe mean for each activity of current db
def calculate_fe_for_current_db(data, current_db_activities):
    fe = {}

    for current_db_activity in current_db_activities:
        features = []
        data_activity = [sample for sample in data if sample.get('label') == current_db_activity]

        if len(data_activity) > 0:

            for sample in data_activity:
                if isinstance(sample.get('fe'), list):
                    features.append(sample.get('fe'))

            feature_avg = np.array(features)
            feature_avg = np.mean(feature_avg, axis=0)

        else:
            feature_avg = 0

        fe[current_db_activity] = feature_avg

    return fe


def calculate_edit_distance_for_current_db(current_db_activities, correct_labels):
    edit_distances = {}
    for label in correct_labels:
        for original_activity in current_db_activities:
            dist = iterative_levenshtein(original_activity, label)
            edit_distances[original_activity + '-' + label] = dist
    return edit_distances


def compute_euclidean_distance(correct_label_fe, current_db_activity_fe):
    a1 = np.array(correct_label_fe)
    a2 = np.array(current_db_activity_fe)

    difference = (a1 - a2) ** 2
    dist = np.sum(difference)
    dist = np.sqrt(dist)

    return dist


def calculate_confusion_matrix(fe):
    len(fe.keys())
    max = 0
    for k in fe.keys():
        for k2 in fe.keys():
            dist = compute_euclidean_distance(fe.get(k), fe.get(k2))
            if dist > max:
                max = dist
    print("max: %s" % max)
    return max



def on_get_labels_comparison_received(db_id):
    data, dbinfo = get_all_data_to_be_uniformed(db_id)
    if not dbinfo:
        return "Error"
    current_db_activities = dbinfo.get('activities')
    correct_labels = get_all_labels()
    correct_labels_list = correct_labels.keys()

    fe = calculate_fe_for_current_db(data, current_db_activities)
    max_thresold = calculate_confusion_matrix(fe)
    edit_distances = calculate_edit_distance_for_current_db(current_db_activities, correct_labels_list)

    result = {'db_id': db_id}
    labels = {}

    for current_db_activity in current_db_activities:
        current_db_activity_fe = fe.get(current_db_activity)
        similar_labels = []

        for correct_label in correct_labels:
            correct_label_fe = correct_labels.get(correct_label).get('fe_avg')

            # Euclidean distance
            dist = compute_euclidean_distance(correct_label_fe, current_db_activity_fe)
            print(current_db_activity + " - " + correct_label + ": " + str(dist))

            # matplotlib.use('TkAgg')
            # plt.plot(correct_label_fe)
            # plt.plot(current_db_activity_fe)
            # plt.show()

            if dist <= max_thresold or edit_distances.get(current_db_activity+'-'+correct_label) < EDIT_DISTANCE_THRESHOLD:
                if math.isnan(dist):
                    dist = None
                else:
                    dist = round(dist, 4)
                similar_labels.append({
                    "label": correct_label,
                    "fe_distance": dist,
                    "edit_dist": edit_distances.get(current_db_activity+'-'+correct_label)
                })
        labels[current_db_activity] = similar_labels
    result['labels'] = labels
    result['type'] = 'labels_comparison'

    do_graph(db_id, data, correct_labels, result)
    return result

def uniform_signal(db_id, values):
    import yaml
    from pymongo import MongoClient
    from Utils.enums import Config
    with open("config.yml", 'r') as yml_file:
        config = yaml.safe_load(yml_file)
    mongo_connection_data = config[Config.MONGO_CONNECTION.value]
    client = MongoClient(mongo_connection_data[Config.HOST.value], mongo_connection_data[Config.PORT.value])
    db_connection = client[mongo_connection_data[Config.DB_NAME.value]]

    sensors = db_connection["dbinfo_to_be_uniformed"].find({"db_id":db_id})[0]

    config = db_connection["config"].find({"id":0})[0]

    return start_uniform(sensors['sensors'][0], values, config['frequency'], config['uom']['accelerometer'])


def do_graph(db_id, data, correct_labels, result):
    if not os.path.exists(os.path.join(graph_path, db_id, "")):
        os.makedirs(os.path.join(graph_path, db_id, ""))

    original_labels = result.get('labels')

    for label in original_labels:
        current_db_magnitude = [sample for sample in data if sample.get('label') == label]
        if len(current_db_magnitude) > 1:
            current_db_magnitude = current_db_magnitude[randrange(len(current_db_magnitude))]
            matching_labels = original_labels.get(label)
            
            for matching_label in matching_labels:
                # matching_label = correct label
                matching_label_magnitude = correct_labels.get(matching_label.get('label')).get('magnitude')

                sns.set_style("darkgrid")

                if len(current_db_magnitude.get('magnitude')) <= len(matching_label_magnitude):
                    len_min = len(current_db_magnitude.get('magnitude'))
                else:
                    len_min = len(matching_label_magnitude)

                plt.plot(uniform_signal(db_id, current_db_magnitude.get('magnitude'))[0:len_min]) # to uniform
                plt.plot(matching_label_magnitude[0:len_min])
                plt.xlabel("X")
                plt.ylabel("Intensity")
                plt.title(db_id + '/' + label.replace('/','-') + '_' + matching_label.get('label').replace('/','-'))

                plt.savefig(graph_path + '/' + db_id + '/' + label.replace('/','-') + '_' + matching_label.get('label').replace('/','-') + '.png')
                plt.clf()


def get_labels():
    dbinfos = get_all_dbinfo()
    labels = []
    for dbinfo in dbinfos:
        current_activities = dbinfo.get('activities')
        for act in current_activities:
            if act not in labels:
                labels.append(act)

    if len(labels) == 0:
        labels = ["standing", "sitting", "laying", "walking", "walking downstairs", "walking upstairs", "stand to sit",
                  "sit to stand", "sit to lie", "lie to sit", "stand to lie", "lie to stand"]

    return labels


def on_get_db_ready_to_bo_uniformed_received():
    db_to_be_uniformed = get_all_list_data('dbinfo_to_be_uniformed')

    dbs_dict = {'type': 'dbs_be_uniformed'}
    dbs = []
    for db in db_to_be_uniformed:
        if db.get('status') == 1:
            dbs.append(db.get('db_id'))
    dbs_dict['dbs'] = dbs
    return dbs_dict


def iterative_levenshtein(s, t, costs=(1, 1, 1)):
    """
        iterative_levenshtein(s, t) -> ldist
        ldist is the Levenshtein distance between the strings
        s and t.
        For all i and j, dist[i,j] will contain the Levenshtein
        distance between the first i characters of s and the
        first j characters of t

        costs: a tuple or a list with three integers (d, i, s)
               where d defines the costs for a deletion
                     i defines the costs for an insertion and
                     s defines the costs for a substitution
    """

    rows = len(s) + 1
    cols = len(t) + 1
    deletes, inserts, substitutes = costs

    dist = [[0 for x in range(cols)] for x in range(rows)]

    # source prefixes can be transformed into empty strings
    # by deletions:
    for row in range(1, rows):
        dist[row][0] = row * deletes

    # target prefixes can be created from an empty source string
    # by inserting the characters
    for col in range(1, cols):
        dist[0][col] = col * inserts

    for col in range(1, cols):
        for row in range(1, rows):
            if s[row - 1] == t[col - 1]:
                cost = 0
            else:
                cost = substitutes
            dist[row][col] = min(dist[row - 1][col] + deletes,
                                 dist[row][col - 1] + inserts,
                                 dist[row - 1][col - 1] + cost)  # substitution

    return dist[row][col]

def go_up(path, n):
    for i in range(n):
        path = os.path.dirname(path)
    return path

def on_get_graph_comparison_received(db_name):
    current = os.getcwd()
    current = go_up(current,2)

    if(os.path.split(current)[-1] == 'CLP_RepositoryManagement'):
        path_graph = os.path.join(current,"Data","Graph")
    else:
        path_graph = os.path.join("Data","Graph")

    try:
        graphs = os.listdir(os.path.join(path_graph,db_name))
        graphs_data = {}

        for graph in graphs:
            with open(os.path.join(path_graph,db_name,graph), "rb") as image_file:
                encoded_graph = base64.b64encode(image_file.read())
            graphs_data[graph[:-4]] = encoded_graph.decode("utf-8")

        return graphs_data
    except:
        return {}