import yaml
import json
from pymongo import MongoClient

from Core.manager.query_manager import setup_connection_query_repo
from Utils.enums import Config
from Core.manager.model_manager import setup_connection_model_manager


def init_db_connection():
    global db_connection
    with open("config.yml", 'r') as yml_file:
        config = yaml.safe_load(yml_file)

    # Crate connection with DB
    mongo_connection_data = config[Config.MONGO_CONNECTION.value]
    client = MongoClient(mongo_connection_data[Config.HOST.value], mongo_connection_data[Config.PORT.value])
    db_connection = client[mongo_connection_data[Config.DB_NAME.value]]

    setup_connection_query_repo(db_connection)
    
    #Chiamata a funzione del model_manager per impostare la connessione.
    #Il modulo model_manger si occupa di gestire i messaggi ricevuti dal DataDistribution che 
    #riguardano la richiesta di informazioni sui modelli
    setup_connection_model_manager(db_connection)


def save_dbinfo(collection, db_info):
    db_info_from_db = db_connection[collection].find_one_and_delete({"db_name": db_info.get("db_name")})

    # If the db is already present the DB
    if db_info_from_db:
        delete_all_by_id(db_info.get("db_name"))

    db_connection[collection].insert_one(db_info)


def update_dbinfo_status(db_info):
    db_info_from_db = db_connection['dbinfo_to_be_uniformed'].find_one_and_delete({"db_name": db_info.get("db_name")})
    db_info_from_db['status'] = 1

    db_connection['dbinfo_to_be_uniformed'].insert_one(db_info)


def save_data(data, collection):
    if data.get("progress"):
        progress = str(data.get("progress")).split("/")

        if int(progress[0]) % 50 == 0:
            print(data.get("progress"))

        del data['progress']

    try:
        db_connection[collection].insert_one(data)
    except:
        data2 = data
        sensors = ['accelerometer','gyroscope','magnetometer']

        for sensor in sensors:
            try:
                n = int(round(len(data[sensor])/2))
                data[sensor] = data[sensor][:n]
                data2[sensor] = data2[sensor][n:]
            except:
                pass

        db_connection[collection].insert_one(data)

        import bson
        data2['_id'] = bson.objectid.ObjectId()
        db_connection[collection].insert_one(data2)


def get_single_data(collection_name, id):
    return db_connection[collection_name].find_one({"db_id": id}, {'_id': False})


def get_all_list_data(collection_name):
    return list(db_connection[collection_name].find({}, {'_id': False}))


def get_list_data(collection_name, id):
    return list(db_connection[collection_name].find({"db_id": id}, {'_id': False}))


def delete_all_data(db_id, collection_name):
    db_connection[collection_name].delete_many({"db_id": db_id})


def get_dbinfo_to_be_uniformed(db_id):
    return db_connection["dbinfo_to_be_uniformed"].find_one({"db_id": db_id}, {'_id': False})


def get_all_data_to_be_uniformed(db_id):
    all_data = db_connection["uniformed_frequency"].find({"db_id": db_id}, {'_id': False})
    dbinfo = db_connection["dbinfo_to_be_uniformed"].find_one({"db_id": db_id}, {'_id': False})
    return list(all_data), dbinfo


def get_all_data_to_be_uniformed_by_id(db_id):

    all_data = db_connection["imported_temp"].find({"db_id": db_id}, {'_id': False})
    dbinfo = db_connection["dbinfo_to_be_uniformed"].find_one({"db_id": db_id}, {'_id': False})

    return list(all_data), dbinfo

def get_all_data_by_id(db_id):
    all_data = db_connection["imported"].find({"db_id": db_id}, {'_id': False})
    dbinfo = db_connection["dbinfo_to_be_uniformed"].find_one({"db_id": db_id}, {'_id': False})
    return list(all_data), dbinfo

def get_all_data_by_id_uniformed(db_id):
    all_data = db_connection["imported"].find({"db_id": db_id}, {'_id': False})
    dbinfo = db_connection["dbinfo"].find_one({"db_id": db_id}, {'_id': False})
    return list(all_data), dbinfo


def get_all_config():
    return list(db_connection["config"].find({}, {'_id': False}))


def get_all_dbinfo():
    return list(db_connection["dbinfo"].find({}, {'_id': False}))


def get_default_config():
    return db_connection["config"].find_one({'id': 0}, {'_id': False})


def insert_new_config(data):
    data['id'] = db_connection['config'].count()
    db_connection['config'].insert_one(data)
    del data['_id']
    return data


def insert_one(collection, data):
    db_connection[collection].insert_one(data)


def get_all_data():
    return list(db_connection['imported'].find({}, {'_id': 0}))


def get_sensor_by_db_id(db_id):
    return db_connection['dbinfo'].find_one({'db_id': db_id}, {'_id': 0, 'sensors': 1})


def create_default_config():
    default = db_connection["config"].find_one({"id": 0})
    if not default:
        config = {
            "id":  0,
            "frequency": 50,
            "uom": {
                "accelerometer": "m/s^2",
                "gyroscope": "rad/s",
                "magnetometer": "microTesla",
                "orientation": "dx"
            }
        }
        db_connection["config"].insert_one(config)


def delete_all_by_id(db_id):
    delete_all_data(db_id, "dbinfo")
    delete_all_data(db_id, "imported")
    delete_all_data(db_id, "imported_temp")
    delete_all_data(db_id, "dbinfo_to_be_uniformed")
    delete_all_data(db_id, "uniformed_frequency")

    configs = get_all_config()
    for config in configs:
        id = config.get('id')
        delete_all_data(db_id, 'uniformed_' + str(id))


def get_all_labels():
    return db_connection['labels'].find_one({}, {'_id': False})


def find_one_and_delete_labels():
    return db_connection['labels'].find_one_and_delete({}, {'_id': 0})
    #return db_connection['labels'].find_one({}, {'_id': 0})


'''
def get_one_data_by_label(db_id):
    data = []
    labels = list(db_connection['dbinfo'].find_one({'db_id': db_id}).get('activities'))
    for label in labels:
        labeled_data = db_connection['imported'].find_one({'db_id': db_id, 'label': label})
        temp = {'accelerometer': labeled_data.get('accelerometer'),
                'label': labeled_data.get('label'),
                'db_id': labeled_data.get('db_id')
                }
        data.append(temp)
    return data


def get_accelerometer_frequency(db_id):
    sensors = db_connection['dbinfo'].find_one({'db_id': db_id}, {'sensors': 1})
    for sensor in sensors.get('sensors'):
        if sensor.get('name') == 'accelerometer':
            return sensor.get('frequency')
'''


def on_get_dbnames_received():
    dbinfos = get_all_dbinfo()
    names = []
    for db_info in dbinfos:
        names.append(db_info.get('db_id'))
    return names


def on_get_labels_received():
    labels = db_connection['labels'].find_one({}, {'_id': False})
    if labels:
        return list(labels.keys())
    else:
        return []

