import json
import pika

from Core.MongoDB.mongo_repository import delete_all_by_id
from Core.manager.config_manager import on_new_config_request_received
from Core.manager.importer_manager import on_imported_data_default_request_received, on_uniformed_default_data_received, \
    on_dbinfo_received, on_imported_data_received, on_data_end_importing_received, \
    on_end_uniformed_default_data_received, normalize_data_received, normalize_range
from Core.manager.query_manager import on_query_request_received
from Core.manager.uniform_manager import on_uniform_request_received, on_imported_data_request_received, \
    on_uniformed_data_received
from Core.RabbitMQ.message_sender import init_channel
from Utils.enums import RequestType, KafkaDataMessageType
from Core.manager.model_manager import get_model, put_model


#######################################################################################################################
#                                           RABBITMQ CALLBACKS
#######################################################################################################################
# Message received from repository
def imported_data_callback(ch, method, properties, body):
    data = json.loads(body)
    data_type = data['type']
    if data_type == KafkaDataMessageType.DB_INFO.value:
        on_dbinfo_received(data)

    elif data_type == KafkaDataMessageType.IMPORTED.value:
        on_imported_data_received(data)

    elif data_type == KafkaDataMessageType.END_IMPORTING.value:
        on_data_end_importing_received(data)


def data_callback(ch, method, properties, body):
    data = json.loads(body)
    data_type = data['type']

    if data_type == KafkaDataMessageType.UNIFORMED_DEFAULT.value:
        on_uniformed_default_data_received(data)

    elif data_type == RequestType.GET_IMPORTED_DATA_DEFAULT.value:
        on_imported_data_default_request_received(data)

    elif data_type == KafkaDataMessageType.UNIFORMED.value:
        on_uniformed_data_received(data)

        # normalize_data_received(data)
        #normalize_range(data)

    elif data_type == 'end_uniforming_default':
        on_end_uniformed_default_data_received(data)


def request_callback(ch, method, properties, body):
    request = json.loads(body)
    print(request)
    request_type = request['type']

    if request_type == RequestType.GET_IMPORTED_DATA.value:
        on_imported_data_request_received(request)

    elif request_type == RequestType.UNIFORM_LABELS.value:
        on_uniform_request_received(request)

    elif request_type == RequestType.NEW_CONFIG.value:
        del request['type']
        on_new_config_request_received(request)

    elif request_type == RequestType.DELETE.value:
        db_name = request.get("db_name")
        delete_all_by_id(db_name)

    elif request_type == 'query':
        filename = on_query_request_received(request)
        channel.basic_publish(exchange='', routing_key='json_filename', body=json.dumps(filename))


#Funzione di callback chiamata quando avviene una consumazione dalla coda 'req_model_to_repo'.
def req_model_callback(ch, method, props, body):
    request = json.loads(body)
    print(request)
    request_type = request['type'] #Estrazione del tipo di richiesta dalla richiesta

    if request_type == RequestType.MODEL_GET.value: #model_get
        model_info = get_model(request) #chiamata a funzione get_model situata in Core.manager.model_manager

        #Invio del messaggio sulla coda di risposta 'reply_queue' con aggiunta del correlation_id ricevuto dal
        #richiedente per far sì che il messaggio venga restituito al richiedente corretto
        channel.basic_publish(exchange='', routing_key='reply_queue', 
                            properties=pika.BasicProperties(correlation_id = props.correlation_id), 
                            body=json.dumps(model_info))
    elif request_type == RequestType.MODEL_PUT.value: #model_put
        result = put_model(request) #chiamata a funzione put_model situata in Core.manager.model_manager
        channel.basic_publish(exchange='', routing_key='reply_queue', 
                            properties=pika.BasicProperties(correlation_id = props.correlation_id), 
                            body=json.dumps(result))


def init_rabbitmq(rabbitmq_config):
    global channel
    credentials = pika.PlainCredentials(rabbitmq_config.get('user'), rabbitmq_config.get('password'))
    parameters = pika.ConnectionParameters(rabbitmq_config.get('host'),
                                           rabbitmq_config.get('port'),
                                           '/',
                                           heartbeat=0,
                                           credentials=credentials)
    


    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()


    channel.queue_declare(queue='imported_data')
    channel.queue_declare(queue='data_to_repo')
    #channel.queue_declare(queue='data_to_dm')
    channel.queue_declare(queue='req_to_repo')
    channel.queue_declare(queue='json_filename')

    #Dichiarazione code necessarie per la comunicazione con il DataDistribution(DD).
    #req_model_to_repo è la coda dalla quale arrivano le richieste dal DD
    #reply_queue è la coda sulla quale vengono mandati i messaggi al DD
    channel.queue_declare(queue='req_model_to_repo')
    channel.queue_declare(queue='reply_queue')



    #channel.basic_qos(prefetch_count=1)

    channel.basic_consume(queue='imported_data', on_message_callback=imported_data_callback, auto_ack=True)
    channel.basic_consume(queue='data_to_repo', on_message_callback=data_callback, auto_ack=True)
    channel.basic_consume(queue='req_to_repo', on_message_callback=request_callback, auto_ack=True)

    #Quando avviene la consumazione del messaggio dalla coda 'req_model_to_repo' significa che dal DD è arrivata
    #una richiesta di qualche tipo, viene quindi chiamata la callback 'req_model_callback' che si trova a riga 79
    #di questo modulo
    channel.basic_consume(queue='req_model_to_repo', on_message_callback=req_model_callback, auto_ack=True)

    init_channel(channel)
    try:
        channel.basic_qos(prefetch_count=10)
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()
    connection.close()
