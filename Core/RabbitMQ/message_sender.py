import json

channel = None


def init_channel(ch):
    global channel
    channel = ch


def send_message(routing_key, message):
    channel.basic_publish(exchange='', routing_key=routing_key, body=json.dumps(message))
