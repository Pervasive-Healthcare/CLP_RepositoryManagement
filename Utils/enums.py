from enum import Enum


class Config(Enum):
    RABBIT_MQ_CONNECTION = "rabbitmq_connection"
    MONGO_CONNECTION = "mongodb_connection"
    KAFKA_CONNECTION = "kafka_connection"
    POSTGRES_CONNECTION = "postgress_connection"
    DB_NAME = "db_name"
    HOST = "host"
    PORT = "port"


class CollectionsName(Enum):
    IMPORTED = "Imported"
    CONFIG = "Config"
    UNIFORMED = "Uniformed"





class RequestType(Enum):
    DELETE = "delete"
    GET_IMPORTED_DATA = "request_imported_data"
    GET_IMPORTED_DATA_DEFAULT = "request_imported_data_default"

    GET_IMPORTED_DATA_BY_LABEL = "request_imported_data_by_label"
    QUERY = "query"
    NEW_CONFIG = "new_config"
    UNIFORM_LABELS = "uniform_label"

    MODEL_GET = "model_get"
    MODEL_PUT = "model_put"


class KafkaDataMessageType(Enum):
    TYPE = "type"
    DB_INFO = "config"
    IMPORTED = "imported"
    END_IMPORTING = "end_importing"
    UNIFORMED = "uniformed"
    UNIFORMED_DEFAULT = "uniformed_default"
    QUERY = "query"
